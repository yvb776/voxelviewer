void extractmodel()
{
	static char idxfile[120];
	static char idyfile[120];
	static char idzfile[120];
	static char statusfile[120];
	FILE *fpidx;
	FILE *fpidy;
	FILE *fpidz;
	
	int cnt = 0;
	sprintf(idxfile, "idx.dat");
	sprintf(idyfile, "idy.dat");
	sprintf(idzfile, "idz.dat");
	if ((fpidx = fopen(idxfile, "w")) == NULL || (fpidy = fopen(idyfile, "w")) == NULL || (fpidz = fopen(idzfile, "w")) == NULL) {
		//		printf("File open error\n");
		exit(1);
	}
	for (int k = 1; k <= NZ - 1; k++){
		for (int j = 1; j <= NY - 1; j++){
			for (int i = 1; i <= NX - 1; i++){
				fprintf(fpidx, "%d ", idx[i][j][k]);
				fprintf(fpidy, "%d ", idy[i][j][k]);
				fprintf(fpidz, "%d ", idz[i][j][k]);
			}
			fprintf(fpidx, "\n");
			fprintf(fpidy, "\n");
			fprintf(fpidz, "\n");
		}
		cnt++;
	}
	fclose(fpidx);
	fclose(fpidy);
	fclose(fpidz);

	sprintf(statusfile, "Status.dat");
	if ((fpidx = fopen(statusfile, "w")) == NULL) {
		//		printf("File open error\n");
		exit(1);
	}
	fprintf(fpidx, "%d\n", NY);
	fprintf(fpidx, "%d\n", NZ);

	fclose(fpidx);

}
