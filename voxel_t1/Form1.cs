﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Dialogs;

using System.Threading;


namespace voxel_t1
{
	public partial class voxel_t1 : Form
	{
		string imgX, imgY, imgZ;
		bool isFileOpened = false;
		bool isSavingX = false;
		bool isSavingY = false;
		bool isSavingZ = false;
		bool isOpeningX = false;
		bool isOpeningY = false;
		bool isOpeningZ = false;
		bool isInit = false;
		bool isDrawingX = false;
		bool isDrawingY = false;
		bool isDrawingZ = false;
		byte[, ,] voxelDataX;
		byte[, ,] voxelDataY;
		byte[, ,] voxelDataZ;

		string[] space = { " " };

		System.IO.Stream fileidx;
		System.IO.Stream fileidy;
		System.IO.Stream fileidz;
		System.IO.Stream fileStatus;
		System.IO.StreamReader voxelStreamX;
		System.IO.StreamReader voxelStreamY;
		System.IO.StreamReader voxelStreamZ;
		System.IO.StreamReader voxelStatusStream;


		string tmpX;
		string[] separetedX;

		string tmpY;
		string[] separetedY;

		string tmpZ;
		string[] separetedZ;

		const int far_color = 40;

		Color bgColor = Color.FromArgb(0, 0, 0);

		Bitmap voxelImgX, voxelImgY, voxelImgZ;

		ParallelOptions option = new ParallelOptions() { MaxDegreeOfParallelism = Environment.ProcessorCount };

		System.Diagnostics.Stopwatch swX = new System.Diagnostics.Stopwatch();
		System.Diagnostics.Stopwatch swY = new System.Diagnostics.Stopwatch();
		System.Diagnostics.Stopwatch swZ = new System.Diagnostics.Stopwatch();


		public voxel_t1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{

		}

		private void btOpen_Click(object sender, EventArgs e)
		{
			chkDrawMode.Enabled = false;
			numX.Enabled = false;
			numY.Enabled = false;
			numZ.Enabled = false;
			numGuard.Enabled = false;
			numY_cur.Maximum = numY.Value + numGuard.Value * 2 - 1;
			numZ_cur.Maximum = numZ.Value + numGuard.Value * 2 - 1;



			if (ofdX.ShowDialog() == DialogResult.OK && ofdY.ShowDialog() == DialogResult.OK && ofdZ.ShowDialog() == DialogResult.OK && ofdStatus.ShowDialog() == DialogResult.OK)
			{
				fileidx = ofdX.OpenFile();
				fileidy = ofdY.OpenFile();
				fileidz = ofdZ.OpenFile();
				fileStatus = ofdStatus.OpenFile();

				isOpeningX = isOpeningY = isOpeningZ = true;

				if (fileidx != null && fileidy != null && fileidx != null && fileStatus != null)
				{
					swX.Reset();
					swY.Reset();
					swZ.Reset();
					swX.Start();
					swY.Start();
					swZ.Start();

					isFileOpened = true;

					voxelStreamX = new System.IO.StreamReader(fileidx);
					voxelStreamY = new System.IO.StreamReader(fileidy);
					voxelStreamZ = new System.IO.StreamReader(fileidz);
					voxelStatusStream = new System.IO.StreamReader(fileStatus);

					tmpX = voxelStreamX.ReadLine();
					separetedX = tmpX.Split(space, StringSplitOptions.RemoveEmptyEntries);

					tmpY = voxelStreamY.ReadLine();
					separetedY = tmpY.Split(space, StringSplitOptions.RemoveEmptyEntries);

					tmpZ = voxelStreamZ.ReadLine();
					separetedZ = tmpZ.Split(space, StringSplitOptions.RemoveEmptyEntries);

					string Status = voxelStatusStream.ReadLine();
					numY.Enabled = false;
					numY.Value = Int32.Parse(Status) - 1 - Decimal.ToInt32(numGuard.Value) * 2;//1行目NY
					numY_cur.Maximum = numY.Value + numGuard.Value * 2 - 1;

					Status = voxelStatusStream.ReadLine();
					numZ.Enabled = false;
					numZ.Value = Int32.Parse(Status) - 1 - Decimal.ToInt32(numGuard.Value) * 2;//2行目NZ
					numZ_cur.Maximum = numZ.Value + numGuard.Value * 2 - 1;

					numX.Enabled = false;
					numX.Value = separetedX.Length - Decimal.ToInt32(numGuard.Value) * 2;
					numX_cur.Maximum = numX.Value + numGuard.Value * 2 - 1;

					voxelDataX = new byte[separetedX.Length, Decimal.ToInt32(numY.Value + (numGuard.Value * 2)), Decimal.ToInt32(numZ.Value + (numGuard.Value * 2))];
					voxelDataY = new byte[separetedX.Length, Decimal.ToInt32(numY.Value + (numGuard.Value * 2)), Decimal.ToInt32(numZ.Value + (numGuard.Value * 2))];
					voxelDataZ = new byte[separetedX.Length, Decimal.ToInt32(numY.Value + (numGuard.Value * 2)), Decimal.ToInt32(numZ.Value + (numGuard.Value * 2))];

					bgOpenX.RunWorkerAsync();
					bgOpenY.RunWorkerAsync();
					bgOpenZ.RunWorkerAsync();
#if false
					//ParallelOptions option = new ParallelOptions();
					//option.MaxDegreeOfParallelism = 3;
					//Parallel.Invoke(option,
					var task = new[]
					{
						Task.Factory.StartNew(
							() =>
							{
								for (int z = 0; z < numZ.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamX.EndOfStream; z++)
								{
									for (int y = 0; y < numY.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamX.EndOfStream ; y++)
									{
										for (int x = 0; x < numX.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamX.EndOfStream ; x++)
										{
											if (x < separetedX.Length)
												voxelDataX[x, y, z] = Byte.Parse(separetedX[x]);
										}
										tmpX = voxelStreamX.ReadLine();
										separetedX = tmpX.Split(space, StringSplitOptions.RemoveEmptyEntries);
									}
								}
							}),
						Task.Factory.StartNew(
							() =>
							{
								for (int z = 0; z < numZ.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamY.EndOfStream; z++)
								{
									for (int y = 0; y < numY.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamY.EndOfStream; y++)
									{
										for (int x = 0; x < numX.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamY.EndOfStream ; x++)
										{
											if (x < separetedY.Length)
											voxelDataY[x, y, z] = Byte.Parse(separetedY[x]);
										}
										tmpY = voxelStreamY.ReadLine();
										separetedY = tmpY.Split(space, StringSplitOptions.RemoveEmptyEntries);
									}
								}
							}),
						Task.Factory.StartNew(
							() =>
							{
								for (int z = 0; z < numZ.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamZ.EndOfStream; z++)
								{
									for (int y = 0; y < numY.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamZ.EndOfStream; y++)
									{
										for (int x = 0; x < numX.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamZ.EndOfStream; x++)
										{
											if (x < separetedZ.Length)
											voxelDataZ[x, y, z] = Byte.Parse(separetedZ[x]);
										}
										tmpZ = voxelStreamZ.ReadLine();
										separetedZ = tmpZ.Split(space, StringSplitOptions.RemoveEmptyEntries);
									}
								}
							})
					};
					Task.WaitAll(task);

				}

				fileidx.Close();
				fileidy.Close();
				fileidz.Close();
				fileStatus.Close();
				btDraw.Enabled = true;
				btSave.Enabled = true;
				voxelImgY = new Bitmap(Decimal.ToInt32(numX.Value + numGuard.Value * 2), Decimal.ToInt32(numZ.Value + numGuard.Value * 2));
				voxelImgX = new Bitmap(Decimal.ToInt32(numY.Value + numGuard.Value * 2), Decimal.ToInt32(numZ.Value + numGuard.Value * 2));
				voxelImgZ = new Bitmap(Decimal.ToInt32(numX.Value + numGuard.Value * 2), Decimal.ToInt32(numY.Value + numGuard.Value * 2));
#endif
				}
			}
		}

		private void bgOpenX_DoWork(object sender, DoWorkEventArgs e)
		{
			BackgroundWorker bgWorker = (BackgroundWorker)sender;

			for (int z = 0; z < numZ.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamX.EndOfStream; z++)
			{
				for (int y = 0; y < numY.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamX.EndOfStream; y++)
				{
					Parallel.For(0, Convert.ToInt32(numX.Value + numGuard.Value * 2), option, x =>//int x = 0; x < numX.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamX.EndOfStream; x++)
					{
						if (x < separetedX.Length)
							voxelDataX[x, y, z] = Byte.Parse(separetedX[x]);
					});
					tmpX = voxelStreamX.ReadLine();
					separetedX = tmpX.Split(space, StringSplitOptions.RemoveEmptyEntries);
				}
				bgWorker.ReportProgress((int)(z / (numZ.Value + numGuard.Value) * 100));
			}

		}

		private void bgOpenX_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			lblPersentageX.Text = e.ProgressPercentage.ToString();
			pbX.Value = e.ProgressPercentage;
		}

		private void bgOpenX_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if (e.Error != null)
				MessageBox.Show("ERROR!(In bgOpenX_RunWorkerCompleted)");
			pbX.Value = 100;
			lblPersentageX.Text = 100.ToString();
			isOpeningX = false;
			swX.Stop();
			lblSWX.Text = (swX.ElapsedMilliseconds.ToString() + " ms");
			if (!(isOpeningX || isOpeningY || isOpeningZ || isInit))
			{
				isInit = true;
				fileidx.Close();
				fileidy.Close();
				fileidz.Close();
				fileStatus.Close();
				voxelStreamX.Close();
				voxelStreamY.Close();
				voxelStreamZ.Close();
				voxelStatusStream.Close();
				btDraw.Enabled = true;
				btSave.Enabled = true;
				chkDrawMode.Enabled = true;
				voxelImgY = new Bitmap(Decimal.ToInt32(numX.Value + numGuard.Value * 2), Decimal.ToInt32(numZ.Value + numGuard.Value * 2));
				voxelImgX = new Bitmap(Decimal.ToInt32(numY.Value + numGuard.Value * 2), Decimal.ToInt32(numZ.Value + numGuard.Value * 2));
				voxelImgZ = new Bitmap(Decimal.ToInt32(numX.Value + numGuard.Value * 2), Decimal.ToInt32(numY.Value + numGuard.Value * 2));
			}
		}

		private void bgOpenY_DoWork(object sender, DoWorkEventArgs e)
		{
			BackgroundWorker bgWorker = (BackgroundWorker)sender;

			for (int z = 0; z < numZ.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamY.EndOfStream; z++)
			{
				for (int y = 0; y < numY.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamY.EndOfStream; y++)
				{
					Parallel.For(0, Convert.ToInt32(numX.Value + numGuard.Value * 2), option, x =>//int x = 0; x < numX.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamY.EndOfStream; x++)
					{
						if (x < separetedY.Length)
							voxelDataY[x, y, z] = Byte.Parse(separetedY[x]);
					});
					tmpY = voxelStreamY.ReadLine();
					separetedY = tmpY.Split(space, StringSplitOptions.RemoveEmptyEntries);
				}
				bgWorker.ReportProgress((int)(z / (numZ.Value + numGuard.Value) * 100));
			}
		}

		private void bgOpenY_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			lblPersentageY.Text = e.ProgressPercentage.ToString();
			pbY.Value = e.ProgressPercentage;
		}

		private void bgOpenY_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if (e.Error != null)
				MessageBox.Show("ERROR!(In bgOpenY_RunWorkerCompleted)");
			pbY.Value = 100;
			lblPersentageY.Text = 100.ToString();
			isOpeningY = false;
			swY.Stop();
			lblSWY.Text = (swY.ElapsedMilliseconds.ToString() + " ms");
			if (!(isOpeningX || isOpeningY || isOpeningZ || isInit))
			{
				isInit = true;
				fileidx.Close();
				fileidy.Close();
				fileidz.Close();
				fileStatus.Close();
				voxelStreamX.Close();
				voxelStreamY.Close();
				voxelStreamZ.Close();
				voxelStatusStream.Close();
				btDraw.Enabled = true;
				btSave.Enabled = true;
				chkDrawMode.Enabled = true;
				voxelImgY = new Bitmap(Decimal.ToInt32(numX.Value + numGuard.Value * 2), Decimal.ToInt32(numZ.Value + numGuard.Value * 2));
				voxelImgX = new Bitmap(Decimal.ToInt32(numY.Value + numGuard.Value * 2), Decimal.ToInt32(numZ.Value + numGuard.Value * 2));
				voxelImgZ = new Bitmap(Decimal.ToInt32(numX.Value + numGuard.Value * 2), Decimal.ToInt32(numY.Value + numGuard.Value * 2));
			}
		}

		private void bgOpenZ_DoWork(object sender, DoWorkEventArgs e)
		{
			BackgroundWorker bgWorker = (BackgroundWorker)sender;

			for (int z = 0; z < numZ.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamZ.EndOfStream; z++)
			{
				for (int y = 0; y < numY.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamZ.EndOfStream; y++)
				{
					Parallel.For(0, Convert.ToInt32(numX.Value + numGuard.Value * 2), option, x =>//int x = 0; x < numX.Value + Decimal.ToInt32(numGuard.Value) * 2 && !voxelStreamZ.EndOfStream; x++)
					{
						if (x < separetedZ.Length)
							voxelDataZ[x, y, z] = Byte.Parse(separetedZ[x]);
					});
					tmpZ = voxelStreamZ.ReadLine();
					separetedZ = tmpZ.Split(space, StringSplitOptions.RemoveEmptyEntries);
				}
				bgWorker.ReportProgress((int)(z / (numZ.Value + numGuard.Value) * 100));
			}
		}

		private void bgOpenZ_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			lblPersentageZ.Text = e.ProgressPercentage.ToString();
			pbZ.Value = e.ProgressPercentage;
		}

		private void bgOpenZ_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if (e.Error != null)
				MessageBox.Show("ERROR!(In bgOpenZ_RunWorkerCompleted)");
			pbZ.Value = 100;
			lblPersentageZ.Text = 100.ToString();
			isOpeningZ = false;
			swZ.Stop();
			lblSWZ.Text = (swZ.ElapsedMilliseconds.ToString() + " ms");
			if (!(isOpeningX || isOpeningY || isOpeningZ || isInit))
			{
				isInit = true;
				fileidx.Close();
				fileidy.Close();
				fileidz.Close();
				fileStatus.Close();
				voxelStreamX.Close();
				voxelStreamY.Close();
				voxelStreamZ.Close();
				voxelStatusStream.Close();
				btDraw.Enabled = true;
				btSave.Enabled = true;
				chkDrawMode.Enabled = true;
				voxelImgY = new Bitmap(Decimal.ToInt32(numX.Value + numGuard.Value * 2), Decimal.ToInt32(numZ.Value + numGuard.Value * 2));
				voxelImgX = new Bitmap(Decimal.ToInt32(numY.Value + numGuard.Value * 2), Decimal.ToInt32(numZ.Value + numGuard.Value * 2));
				voxelImgZ = new Bitmap(Decimal.ToInt32(numX.Value + numGuard.Value * 2), Decimal.ToInt32(numY.Value + numGuard.Value * 2));
			}
		}

		private void numX_ValueChanged(object sender, EventArgs e)
		{
			ResizeImgBox();
		}

		private void numY_ValueChanged(object sender, EventArgs e)
		{
			ResizeImgBox();
		}

		private void numZ_ValueChanged(object sender, EventArgs e)
		{
			ResizeImgBox();
		}

		private void ResizeImgBox()
		{
			this.pbImgZ.SetBounds(pbImgZ.Bounds.X, pbImgZ.Bounds.Y, Decimal.ToInt32(numX.Value + numGuard.Value * 2) * Decimal.ToInt32(numScale.Value), Decimal.ToInt32(numY.Value + numGuard.Value * 2) * Decimal.ToInt32(numScale.Value));
			this.pbImgY.SetBounds(pbImgY.Bounds.X, pbImgZ.Bounds.Y + Decimal.ToInt32(numY.Value + numGuard.Value * 2) * Decimal.ToInt32(numScale.Value) + 3, Decimal.ToInt32(numX.Value + numGuard.Value * 2) * Decimal.ToInt32(numScale.Value), Decimal.ToInt32(numZ.Value + numGuard.Value * 2) * Decimal.ToInt32(numScale.Value));
			this.pbImgX.SetBounds(pbImgY.Bounds.X + Decimal.ToInt32(numX.Value + numGuard.Value * 2) * Decimal.ToInt32(numScale.Value) + 3, pbImgZ.Bounds.Y + Decimal.ToInt32(numY.Value + numGuard.Value * 2) * Decimal.ToInt32(numScale.Value) + 3, Decimal.ToInt32(numY.Value + numGuard.Value * 2) * Decimal.ToInt32(numScale.Value), Decimal.ToInt32(numZ.Value + numGuard.Value * 2) * Decimal.ToInt32(numScale.Value));

			this.lblX.Top = pbImgY.Bounds.Y + Decimal.ToInt32(numZ.Value + numGuard.Value * 2) * Decimal.ToInt32(numScale.Value);
			this.label6.Top = pbImgY.Bounds.Y + Decimal.ToInt32(numZ.Value + numGuard.Value * 2) * Decimal.ToInt32(numScale.Value);
			this.label6.Left = pbImgX.Bounds.X;
			this.lblX.Left = label6.Right + 20;

			this.lblY.Top = pbImgY.Bounds.Y + Decimal.ToInt32(numZ.Value + numGuard.Value * 2) * Decimal.ToInt32(numScale.Value);
			this.label5.Top = pbImgY.Bounds.Y + Decimal.ToInt32(numZ.Value + numGuard.Value * 2) * Decimal.ToInt32(numScale.Value);
			this.label5.Left = pbImgY.Left;
			this.lblY.Left = label5.Right + 20;
		}

		private void btDraw_Click(object sender, EventArgs e)
		{
			DrawBMP();
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (isFileOpened)
			{
				pbImgX.Image = null;
				pbImgY.Image = null;
				pbImgZ.Image = null;
				voxelImgX.Dispose();
				voxelImgY.Dispose();
				voxelImgZ.Dispose();
			}
		}

		private void btClose_Click(object sender, EventArgs e)
		{
			pbImgX.Image = null;
			pbImgY.Image = null;
			pbImgZ.Image = null;

			btDraw.Enabled = false;
			numX.Enabled = true;
			numY.Enabled = true;
			numZ.Enabled = true;
			numGuard.Enabled = true;
		}

		private void numX_cur_ValueChanged(object sender, EventArgs e)
		{
			DrawBMP();
		}

		private void numY_cur_ValueChanged(object sender, EventArgs e)
		{
			DrawBMP();
		}

		private void numZ_cur_ValueChanged(object sender, EventArgs e)
		{
			DrawBMP();
		}

		// HACK: http://anis774.net/codevault/turboargbpixelfilter.html

		private void DrawBMPX(int curX, int maxX, int nY, int nZ)
		{
			if (isFileOpened)
			{
				Graphics graph = Graphics.FromImage(voxelImgX);
				graph.Clear(bgColor);
				if (isSavingX == false)
				{
					graph.DrawLine(Pens.AliceBlue, 0, Decimal.ToInt32(numZ_cur.Value), voxelImgX.Width - 1, Decimal.ToInt32(numZ_cur.Value));
					graph.DrawLine(Pens.AliceBlue, Decimal.ToInt32(numY_cur.Value), 0, Decimal.ToInt32(numY_cur.Value), voxelImgX.Height - 1);
				}
				graph.Dispose();

				BitmapPlus bmpP = new BitmapPlus(voxelImgX);
				bmpP.BeginAccess(); //高速化開始

				if (!chkDrawMode.Checked)
				{
					for (int z = 0; z < nZ + numGuard.Value * 2; z++)
					{
						for (int y = 0; y < nY + numGuard.Value * 2; y++)
						{
							if (voxelDataX[Decimal.ToInt32(maxX) - Decimal.ToInt32(curX), y, z] == 2)
								bmpP.SetPixel(y, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z, Color.FromArgb(255, 0, 0));
							if (voxelDataY[Decimal.ToInt32(maxX) - Decimal.ToInt32(curX), y, z] == 2)
								bmpP.SetPixel(y, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z, ClrBlend.ColorBlend(bmpP.GetPixel(y, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z), Color.FromArgb(0, 255, 0)));
							if (voxelDataZ[Decimal.ToInt32(maxX) - Decimal.ToInt32(curX), y, z] == 2)
								bmpP.SetPixel(y, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z, ClrBlend.ColorBlend(bmpP.GetPixel(y, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z), Color.FromArgb(0, 0, 255)));

							if (chkDrawDielectric.Checked)
							{
								if (voxelDataX[Decimal.ToInt32(maxX) - Decimal.ToInt32(curX), y, z] == 1)
									bmpP.SetPixel(y, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z, Color.FromArgb(255, 0, 0));
								if (voxelDataY[Decimal.ToInt32(maxX) - Decimal.ToInt32(curX), y, z] == 1)
									bmpP.SetPixel(y, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z, ClrBlend.ColorBlend(bmpP.GetPixel(y, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z), Color.FromArgb(0, 255, 0)));
								if (voxelDataZ[Decimal.ToInt32(maxX) - Decimal.ToInt32(curX), y, z] == 1)
									bmpP.SetPixel(y, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z, ClrBlend.ColorBlend(bmpP.GetPixel(y, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z), Color.FromArgb(0, 0, 255)));
							}
						}
					}
				}
				else
				{
					bool[,] isDrawed = new bool[voxelImgX.Width, voxelImgX.Height];

					Parallel.For(0, Convert.ToInt32(nZ + numGuard.Value * 2), option, z =>
					{
						Parallel.For(0, Convert.ToInt32(nY + numGuard.Value * 2), option, y =>
						{
							for (int x = curX; x <= maxX && !isDrawed[y, z]; x++)// HACK: 前は奥から順に描画して、実際に見た感じを出していたが、描画済フラグを使うことで手前から描画してヒットし次第描画中止することで高速化
							{
								if (maxX - curX < 256)// TODO： Color周辺にバグ有 要修正
								{
									if (voxelDataX[maxX - x, y, z] == 2 || voxelDataY[maxX - x, y, z] == 2 || voxelDataZ[maxX - x, y, z] == 2)
									{
										bmpP.SetPixel((int)y, Decimal.ToInt32(nZ + numGuard.Value * 2 - 1 - z), Color.FromArgb(255 - (int)(255 / (maxX - curX)) * x, 255 - (int)(255 / (maxX - curX)) * x, 255 - (int)(255 / (maxX - curX)) * x));
										isDrawed[y, z] = true;
									}
									if (chkDrawDielectric.Checked)
									{
										if (voxelDataX[maxX - x, y, z] == 1 || voxelDataY[maxX - x, y, z] == 1 || voxelDataZ[maxX - x, y, z] == 1)
										{
											bmpP.SetPixel((int)y, Decimal.ToInt32(nZ + numGuard.Value * 2 - 1 - z), Color.FromArgb(0, 255 - (int)(255 / (maxX - curX)) * x, 255 - (int)(255 / (maxX - curX)) * x));
											isDrawed[y, z] = true;
										}
									}
								}
								else
								{
									if (voxelDataX[maxX - x, y, z] == 2 || voxelDataY[maxX - x, y, z] == 2 || voxelDataZ[maxX - x, y, z] == 2)
									{
										//視点(maxX)から250は色を(255-250)まで変えてそれ以降(255-251)

										//240にした
										if (x < 255 - far_color)
											bmpP.SetPixel((int)y, Decimal.ToInt32(nZ + numGuard.Value * 2 - 1 - z), Color.FromArgb(255 - far_color - x, 255 - far_color - x, 255 - far_color - x));
										else
											bmpP.SetPixel((int)y, Decimal.ToInt32(nZ + numGuard.Value * 2 - 1 - z), Color.FromArgb(far_color, far_color, far_color));
										isDrawed[y, z] = true;

									}
									if (chkDrawDielectric.Checked)
									{
										if (voxelDataX[maxX - x, y, z] == 1 || voxelDataY[maxX - x, y, z] == 1 || voxelDataZ[maxX - x, y, z] == 1)
										{
											if (x < 255 - far_color)
												bmpP.SetPixel((int)y, Decimal.ToInt32(nZ + numGuard.Value * 2 - 1 - z), Color.FromArgb(0, 255 - far_color - x, 255 - far_color - x));
											else
												bmpP.SetPixel((int)y, Decimal.ToInt32(nZ + numGuard.Value * 2 - 1 - z), Color.FromArgb(0, far_color, far_color));
											isDrawed[y, z] = true;
										}
									}
								}
							}
						});
					});
				}
				bmpP.EndAccess();

			}
		}

		private void DrawBMPY(int curY, int maxY, int nX, int nZ)
		{
			if (isFileOpened)
			{
				Graphics graph = Graphics.FromImage(voxelImgY);
				graph.Clear(bgColor);
				if (isSavingY == false)
				{
					graph.DrawLine(Pens.AliceBlue, 0, Decimal.ToInt32(numZ_cur.Value), voxelImgY.Width - 1, Decimal.ToInt32(numZ_cur.Value));
					graph.DrawLine(Pens.AliceBlue, voxelImgY.Width - 1 - Decimal.ToInt32(numX_cur.Value), 0, voxelImgY.Width - 1 - Decimal.ToInt32(numX_cur.Value), voxelImgY.Height - 1);
				}
				graph.Dispose();

				BitmapPlus bmpP = new BitmapPlus(voxelImgY);
				bmpP.BeginAccess(); //高速化開始

				if (!chkDrawMode.Checked)
				{
					for (int z = 0; z < nZ + numGuard.Value * 2; z++)
					{
						for (int x = 0; x < nX + numGuard.Value * 2; x++)
						{
							if (voxelDataX[x, curY, z] == 2)
								bmpP.SetPixel(x, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z, Color.FromArgb(255, 0, 0));
							if (voxelDataY[x, curY, z] == 2)
								bmpP.SetPixel(x, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z, ClrBlend.ColorBlend(bmpP.GetPixel(x, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z), Color.FromArgb(0, 255, 0)));
							if (voxelDataZ[x, curY, z] == 2)
								bmpP.SetPixel(x, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z, ClrBlend.ColorBlend(bmpP.GetPixel(x, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z), Color.FromArgb(0, 0, 255)));

							if (chkDrawDielectric.Checked)
							{
								if (voxelDataX[x, curY, z] == 1)
									bmpP.SetPixel(x, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z, Color.FromArgb(255, 0, 0));
								if (voxelDataY[x, curY, z] == 1)
									bmpP.SetPixel(x, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z, ClrBlend.ColorBlend(bmpP.GetPixel(x, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z), Color.FromArgb(0, 255, 0)));
								if (voxelDataZ[x, curY, z] == 1)
									bmpP.SetPixel(x, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z, ClrBlend.ColorBlend(bmpP.GetPixel(x, Decimal.ToInt32(nZ + numGuard.Value * 2) - 1 - z), Color.FromArgb(0, 0, 255)));
							}

						}
					}
				}
				else
				{
					bool[,] isDrawed = new bool[voxelImgY.Width, voxelImgY.Height];

					Parallel.For(0, Convert.ToInt32(nZ + numGuard.Value * 2), option, z =>
					{
						Parallel.For(0, Convert.ToInt32(nX + numGuard.Value * 2), option, x =>
						{
							for (int y = curY; y <= maxY && !isDrawed[x, z]; y++)
							{
								if (maxY - curY < 256)
								{
									if (voxelDataX[x, maxY - y, z] == 2 || voxelDataY[x, maxY - y, z] == 2 || voxelDataZ[x, maxY - y, z] == 2)
									{
										bmpP.SetPixel((int)x, Decimal.ToInt32(nZ + numGuard.Value * 2 - 1 - z), Color.FromArgb(255 - (int)(255 / maxY) * y, 255 - (int)(255 / maxY) * y, 255 - (int)(255 / maxY) * y));
										isDrawed[x, z] = true;
									}
									if (chkDrawDielectric.Checked)
									{
										if (voxelDataX[x, maxY - y, z] == 1 || voxelDataY[x, maxY - y, z] == 1 || voxelDataZ[x, maxY - y, z] == 1)
										{
											bmpP.SetPixel((int)x, Decimal.ToInt32(nZ + numGuard.Value * 2 - 1 - z), Color.FromArgb(0, 255 - (int)(255 / maxY) * y, 255 - (int)(255 / maxY) * y));
											isDrawed[x, z] = true;
										}
									}
								}
								else
								{
									if (voxelDataX[x, maxY - y, z] == 2 || voxelDataY[x, maxY - y, z] == 2 || voxelDataZ[x, maxY - y, z] == 2)
									{
										if (y < 255 - far_color)
											bmpP.SetPixel((int)x, Decimal.ToInt32(nZ + numGuard.Value * 2 - 1 - z), Color.FromArgb(255 - far_color - y, 255 - far_color - y, 255 - far_color - y));
										else
											bmpP.SetPixel((int)x, Decimal.ToInt32(nZ + numGuard.Value * 2 - 1 - z), Color.FromArgb(far_color, far_color, far_color));
										isDrawed[x, z] = true;
									}
									if (chkDrawDielectric.Checked)
									{
										if (voxelDataX[x, maxY - y, z] == 1 || voxelDataY[x, maxY - y, z] == 1 || voxelDataZ[x, maxY - y, z] == 1)
										{
											if (y < 255 - far_color)
												bmpP.SetPixel((int)x, Decimal.ToInt32(nZ + numGuard.Value * 2 - 1 - z), Color.FromArgb(0, 255 - far_color - y, 255 - far_color - y));
											else
												bmpP.SetPixel((int)x, Decimal.ToInt32(nZ + numGuard.Value * 2 - 1 - z), Color.FromArgb(0, far_color, far_color));
											isDrawed[x, z] = true;
										}
									}
								}
							}
						});
					});
				}
				bmpP.EndAccess();

			}
		}

		private void DrawBMPZ(int curZ, int maxZ, int nX, int nY)
		{
			if (isFileOpened)
			{
				Graphics graph = Graphics.FromImage(voxelImgZ);
				graph.Clear(bgColor);
				if (isSavingZ == false)
				{
					graph.DrawLine(Pens.AliceBlue, 0, voxelImgZ.Height - Decimal.ToInt32(numY_cur.Value), voxelImgZ.Width - 1, voxelImgZ.Height - Decimal.ToInt32(numY_cur.Value));
					graph.DrawLine(Pens.AliceBlue, voxelImgZ.Width - 1 - Decimal.ToInt32(numX_cur.Value), 0, voxelImgZ.Width - 1 - Decimal.ToInt32(numX_cur.Value), voxelImgZ.Height - 1);
				}
				graph.Dispose();

				BitmapPlus bmpP = new BitmapPlus(voxelImgZ);
				bmpP.BeginAccess(); //高速化開始

				if (!chkDrawMode.Checked)
				{
					for (int y = 0; y < nY + numGuard.Value * 2; y++)
					{
						for (int x = 0; x < nX + numGuard.Value * 2; x++)
						{
							if (voxelDataX[x, y, maxZ - curZ] == 2)
								bmpP.SetPixel(x, Decimal.ToInt32(nY + numGuard.Value * 2) - 1 - y, Color.FromArgb(255, 0, 0));
							if (voxelDataY[x, y, maxZ - curZ] == 2)
								bmpP.SetPixel(x, Decimal.ToInt32(nY + numGuard.Value * 2) - 1 - y, ClrBlend.ColorBlend(bmpP.GetPixel(x, Decimal.ToInt32(nY + numGuard.Value * 2) - 1 - y), Color.FromArgb(0, 255, 0)));
							if (voxelDataZ[x, y, maxZ - curZ] == 2)
								bmpP.SetPixel(x, Decimal.ToInt32(nY + numGuard.Value * 2) - 1 - y, ClrBlend.ColorBlend(bmpP.GetPixel(x, Decimal.ToInt32(nY + numGuard.Value * 2) - 1 - y), Color.FromArgb(0, 0, 255)));

							if (chkDrawDielectric.Checked)
							{
								if (voxelDataX[x, y, maxZ - curZ] == 1)
									bmpP.SetPixel(x, Decimal.ToInt32(nY + numGuard.Value * 2) - 1 - y, Color.FromArgb(255, 0, 0));
								if (voxelDataY[x, y, maxZ - curZ] == 1)
									bmpP.SetPixel(x, Decimal.ToInt32(nY + numGuard.Value * 2) - 1 - y, ClrBlend.ColorBlend(bmpP.GetPixel(x, Decimal.ToInt32(nY + numGuard.Value * 2) - 1 - y), Color.FromArgb(0, 255, 0)));
								if (voxelDataZ[x, y, maxZ - curZ] == 1)
									bmpP.SetPixel(x, Decimal.ToInt32(nY + numGuard.Value * 2) - 1 - y, ClrBlend.ColorBlend(bmpP.GetPixel(x, Decimal.ToInt32(nY + numGuard.Value * 2) - 1 - y), Color.FromArgb(0, 0, 255)));
							}
						}
					}
				}
				else
				{
					bool[,] isDrawed = new bool[voxelImgZ.Width, voxelImgZ.Height];

					Parallel.For(0, Convert.ToInt32(nY + numGuard.Value * 2), option, y =>//int y = 0; y < nY + numGuard.Value * 2; y++)
					{
						Parallel.For(0, Convert.ToInt32(nX + numGuard.Value * 2), option, x =>//int x = 0; x < nX + numGuard.Value * 2; x++)
						{
							for (int z = curZ; z <= maxZ&&!isDrawed[x,y]; z++)
							{
								if (maxZ - curZ < 256)
								{
									if (voxelDataX[x, y, maxZ - z] == 2 || voxelDataY[x, y, maxZ - z] == 2 || voxelDataZ[x, y, maxZ - z] == 2)
									{
										bmpP.SetPixel((int)x, Decimal.ToInt32(nY + numGuard.Value * 2 - 1 - y), Color.FromArgb(255 - ((int)(255 / maxZ) * z), 255 - ((int)(255 / maxZ) * z), 255 - ((int)(255 / maxZ) * z)));
										isDrawed[x, y] = true;

									}
									if (chkDrawDielectric.Checked)
									{
										if (voxelDataX[x, y, maxZ - z] == 1 || voxelDataY[x, y, maxZ - z] == 1 || voxelDataZ[x, y, maxZ - z] == 1)
										{
											bmpP.SetPixel((int)x, Decimal.ToInt32(nY + numGuard.Value * 2 - 1 - y), Color.FromArgb(0, 255 - ((int)(255 / maxZ) * z), 255 - ((int)(255 / maxZ) * z)));
											isDrawed[x, y] = true;
										}
									}
								}
								else
								{
									if (voxelDataX[x, y, maxZ - z] == 2 || voxelDataY[x, y, maxZ - z] == 2 || voxelDataZ[x, y, maxZ - z] == 2)
									{
										if (z < 255 - far_color)
											bmpP.SetPixel((int)x, Decimal.ToInt32(nY + numGuard.Value * 2 - 1 - y), Color.FromArgb(255 - far_color - z, 255 - far_color - z, 255 - far_color - z));
										else
											bmpP.SetPixel((int)x, Decimal.ToInt32(nY + numGuard.Value * 2 - 1 - y), Color.FromArgb(far_color, far_color, far_color));
										isDrawed[x, y] = true;

									}
									if (chkDrawDielectric.Checked)
									{
										if (voxelDataX[x, y, maxZ - z] == 1 || voxelDataY[x, y, maxZ - z] == 1 || voxelDataZ[x, y, maxZ - z] == 1)
										{
											if (z < 255 - far_color)
												bmpP.SetPixel((int)x, Decimal.ToInt32(nY + numGuard.Value * 2 - 1 - y), Color.FromArgb(0, 255 - far_color - z, 255 - far_color - z));
											else
												bmpP.SetPixel((int)x, Decimal.ToInt32(nY + numGuard.Value * 2 - 1 - y), Color.FromArgb(0, far_color, far_color));
											isDrawed[x, y] = true;
										}
									}
								}

							}
						});
					});
				}
				bmpP.EndAccess();

			}
		}

		private void DrawBMP()
		{
			if (bgDrawX.IsBusy || isDrawingX || isDrawingY || isDrawingZ)
				return;
			else if (isFileOpened)
			{
				swX.Reset();
				swY.Reset();
				swZ.Reset();
				swX.Start();
				swY.Start();
				swZ.Start();

				isDrawingX = true;
				isDrawingY = true;
				isDrawingZ = true;

				pbX.Style = ProgressBarStyle.Marquee;
				pbY.Style = ProgressBarStyle.Marquee;
				pbZ.Style = ProgressBarStyle.Marquee;

				bgDrawX.RunWorkerAsync();
				bgDrawY.RunWorkerAsync();
				bgDrawZ.RunWorkerAsync();
			}
		}

		private void numScale_ValueChanged(object sender, EventArgs e)
		{
			ResizeImgBox();
			DrawBMP();
		}

		private void btSave_Click(object sender, EventArgs e)
		{
			pbImgX.Image = null;
			pbImgY.Image = null;
			pbImgZ.Image = null;

			var Dialog = new CommonOpenFileDialog();

			// フォルダーを開く設定に
			Dialog.IsFolderPicker = true;

			// 読み取り専用フォルダ/コントロールパネルは開かない
			Dialog.EnsureReadOnly = false;
			Dialog.AllowNonFileSystemItems = false;

			// パス指定
			Dialog.DefaultDirectory = Application.StartupPath;

			// 開く
			var Result = Dialog.ShowDialog();

			// もし開かれているなら
			if (Result == CommonFileDialogResult.Ok)
			{
				// ここでいろいろする（開いたフォルダはDialog.FileNameで取得）

				imgX = Dialog.FileName + "\\imgX";
				imgY = Dialog.FileName + "\\imgY";
				imgZ = Dialog.FileName + "\\imgZ";

				DirectoryUtils.SafeCreateDirectory(imgX);
				DirectoryUtils.SafeCreateDirectory(imgY);
				DirectoryUtils.SafeCreateDirectory(imgZ);

				isSavingX = true;
				isSavingY = true;
				isSavingZ = true;

				bgSaveX.RunWorkerAsync();
				bgSaveY.RunWorkerAsync();
				bgSaveZ.RunWorkerAsync();
			}
		}

		private void sfd_FileOk(object sender, CancelEventArgs e)
		{

		}

		private void chkDrawMode_CheckedChanged(object sender, EventArgs e)
		{
			if (chkDrawMode.Checked)
			{
				//numX_cur.Value = 0;
				//numY_cur.Value = 0;
				//numZ_cur.Value = 0;
			}
			DrawBMP();
		}

		private void bgDraw_DoWork(object sender, DoWorkEventArgs e)
		{
			BackgroundWorker bgWorker = (BackgroundWorker)sender;
			if (chkXDraw.Checked)
				DrawBMPX(Decimal.ToInt32(numX_cur.Value), Decimal.ToInt32(numX_cur.Maximum), Decimal.ToInt32(numY.Value), Decimal.ToInt32(numZ.Value));
		}

		private void bgDraw_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			lblPersentageX.Text = e.ProgressPercentage.ToString();
			pbX.Value = e.ProgressPercentage;
		}

		private void bgDraw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			pbX.Style = ProgressBarStyle.Continuous;
			swX.Stop();
			lblSWX.Text = (swX.ElapsedMilliseconds.ToString() + " ms");
			if (e.Error != null)
				MessageBox.Show("ERROR!(bgDraw_RunWorkerCompleted)");
			else
			{
				pbX.Value = 100;
				pbImgX.Interpolation = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
				pbImgX.Image = voxelImgX;


				lblX.Text = "BMP " + voxelImgX.Width + "x" + voxelImgX.Height + "/Form " + pbImgX.Width + "x" + pbImgX.Height + "/2D-Array " + voxelDataX.GetLength(1) + "x" + voxelDataX.GetLength(2);

				isDrawingX = false;
			}
		}

		private void bgDrawY_DoWork(object sender, DoWorkEventArgs e)
		{
			BackgroundWorker bgWorker = (BackgroundWorker)sender;
			if (chkYDraw.Checked)
				DrawBMPY(Decimal.ToInt32(numY_cur.Value), Decimal.ToInt32(numY_cur.Maximum), Decimal.ToInt32(numX.Value), Decimal.ToInt32(numZ.Value));

		}

		private void bgDrawY_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			lblPersentageY.Text = e.ProgressPercentage.ToString();
			pbY.Value = e.ProgressPercentage;
		}

		private void bgDrawY_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			pbY.Style = ProgressBarStyle.Continuous;
			swY.Stop();
			lblSWY.Text = (swY.ElapsedMilliseconds.ToString() + " ms");
			if (e.Error != null)
				MessageBox.Show("ERROR!(bgDrawY_RunWorkerCompleted)");
			else
			{
				pbY.Value = 100;
				pbImgY.Interpolation = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
				pbImgY.Image = voxelImgY;
				lblY.Text = "BMP " + voxelImgY.Width + "x" + voxelImgY.Height + "/Form " + pbImgY.Width + "x" + pbImgY.Height + "/2D-Array " + voxelDataX.GetLength(0) + "x" + voxelDataX.GetLength(2);

				isDrawingY = false;
			}
		}

		private void bgDrawZ_DoWork(object sender, DoWorkEventArgs e)
		{
			BackgroundWorker bgWorker = (BackgroundWorker)sender;
			if (chkZDraw.Checked)
				DrawBMPZ(Decimal.ToInt32(numZ_cur.Value), Decimal.ToInt32(numZ_cur.Maximum), Decimal.ToInt32(numX.Value), Decimal.ToInt32(numY.Value));
		}

		private void bgDrawZ_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			lblPersentageZ.Text = e.ProgressPercentage.ToString();
			pbZ.Value = e.ProgressPercentage;
		}

		private void bgDrawZ_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			pbZ.Style = ProgressBarStyle.Continuous;
			swZ.Stop();
			lblSWZ.Text = (swZ.ElapsedMilliseconds.ToString() + " ms");
			if (e.Error != null)
				MessageBox.Show("ERROR!(bgDrawZ_RunWorkerCompleted)");
			else
			{
				pbZ.Value = 100;
				pbImgZ.Interpolation = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
				pbImgZ.Image = voxelImgZ;
				lblZ.Text = "BMP " + voxelImgZ.Width + "x" + voxelImgZ.Height + "/Form " + pbImgZ.Width + "x" + pbImgZ.Height + "/2D-Array " + voxelDataX.GetLength(0) + "x" + voxelDataX.GetLength(1);

				isDrawingZ = false;
			}
		}

		private void bgSave_DoWork(object sender, DoWorkEventArgs e)
		{
			BackgroundWorker bgWorker = (BackgroundWorker)sender;

			if (!chkDrawMode.Checked)
			{
				for (int cnt = 0; cnt <= numX_cur.Maximum; cnt++)
				{
					bgWorker.ReportProgress((int)(cnt / numX_cur.Maximum * 100));
					DrawBMPX(cnt, Decimal.ToInt32(numX_cur.Maximum), Decimal.ToInt32(numY.Value), Decimal.ToInt32(numZ.Value));
					voxelImgX.Save(imgX + "\\" + cnt + ".png", System.Drawing.Imaging.ImageFormat.Png);
				}
			}
			else
			{
				if (!chkSaveMode.Checked)
				{
					DrawBMPX(Decimal.ToInt32(numX_cur.Value), Decimal.ToInt32(numX_cur.Maximum), Decimal.ToInt32(numY.Value), Decimal.ToInt32(numZ.Value));
					voxelImgX.Save(imgX + ".png", System.Drawing.Imaging.ImageFormat.Png);
				}
				else
				{
					for (int cnt = 0; cnt <= numX_cur.Maximum; cnt++)
					{
						bgWorker.ReportProgress((int)(cnt / numX_cur.Maximum * 100));
						DrawBMPX(cnt, Decimal.ToInt32(numX_cur.Maximum), Decimal.ToInt32(numY.Value), Decimal.ToInt32(numZ.Value));
						voxelImgX.Save(imgX + "\\" + cnt + ".png", System.Drawing.Imaging.ImageFormat.Png);
					}
				}
			}

		}

		private void bgSave_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			lblPersentageX.Text = e.ProgressPercentage.ToString();
			pbX.Value = e.ProgressPercentage;
		}

		private void bgSave_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			isSavingX = false;
		}

		private void bgSaveY_DoWork(object sender, DoWorkEventArgs e)
		{
			BackgroundWorker bgWorker = (BackgroundWorker)sender;

			if (!chkDrawMode.Checked)
			{
				for (int cnt = 0; cnt <= numY_cur.Maximum; cnt++)
				{
					bgWorker.ReportProgress((int)(cnt / numY_cur.Maximum * 100));
					DrawBMPY(cnt, Decimal.ToInt32(numY_cur.Maximum), Decimal.ToInt32(numX.Value), Decimal.ToInt32(numZ.Value));
					voxelImgY.Save(imgY + "\\" + cnt + ".png", System.Drawing.Imaging.ImageFormat.Png);
				}
			}
			else
			{
				if (!chkSaveMode.Checked)
				{
					DrawBMPY(Decimal.ToInt32(numY_cur.Value), Decimal.ToInt32(numY_cur.Maximum), Decimal.ToInt32(numX.Value), Decimal.ToInt32(numZ.Value));
					voxelImgY.Save(imgY + ".png", System.Drawing.Imaging.ImageFormat.Png);
				}
				else
				{
					for (int cnt = 0; cnt <= numY_cur.Maximum; cnt++)
					{
						bgWorker.ReportProgress((int)(cnt / numY_cur.Maximum * 100));
						DrawBMPY(cnt, Decimal.ToInt32(numY_cur.Maximum), Decimal.ToInt32(numX.Value), Decimal.ToInt32(numZ.Value));
						voxelImgY.Save(imgY + "\\" + cnt + ".png", System.Drawing.Imaging.ImageFormat.Png);
					}
				}
			}
		}

		private void bgSaveY_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			lblPersentageY.Text = e.ProgressPercentage.ToString();
			pbY.Value = e.ProgressPercentage;
		}

		private void bgSaveY_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			isSavingY = false;
		}

		private void bgSaveZ_DoWork(object sender, DoWorkEventArgs e)
		{
			BackgroundWorker bgWorker = (BackgroundWorker)sender;

			if (!chkDrawMode.Checked)
			{
				for (int cnt = 0; cnt <= numZ_cur.Maximum; cnt++)
				{
					bgWorker.ReportProgress((int)(cnt / numZ_cur.Maximum * 100));
					DrawBMPZ(cnt, Decimal.ToInt32(numZ_cur.Maximum), Decimal.ToInt32(numX.Value), Decimal.ToInt32(numY.Value));
					voxelImgZ.Save(imgZ + "\\" + cnt + ".png", System.Drawing.Imaging.ImageFormat.Png);
				}
			}
			else
			{
				if (!chkSaveMode.Checked)
				{
					DrawBMPZ(Decimal.ToInt32(numZ_cur.Value), Decimal.ToInt32(numZ_cur.Maximum), Decimal.ToInt32(numX.Value), Decimal.ToInt32(numY.Value));
					voxelImgZ.Save(imgZ + ".png", System.Drawing.Imaging.ImageFormat.Png);
				}
				else
				{
					for (int cnt = 0; cnt <= numZ_cur.Maximum; cnt++)
					{
						bgWorker.ReportProgress((int)(cnt / numZ_cur.Maximum * 100));
						DrawBMPZ(cnt, Decimal.ToInt32(numZ_cur.Maximum), Decimal.ToInt32(numX.Value), Decimal.ToInt32(numY.Value));
						voxelImgZ.Save(imgZ + "\\" + cnt + ".png", System.Drawing.Imaging.ImageFormat.Png);
					}
				}
			}
		}

		private void bgSaveZ_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			lblPersentageZ.Text = e.ProgressPercentage.ToString();
			pbZ.Value = e.ProgressPercentage;
		}

		private void bgSaveZ_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			isSavingZ = false;
		}

		private void btColor_Click(object sender, EventArgs e)
		{
			clDialog.AllowFullOpen = true;
			if (clDialog.ShowDialog() == DialogResult.OK)
			{
				bgColor = clDialog.Color;
			}
			else
			{
				bgColor = Color.FromArgb(0, 0, 0);
			}
		}
	}
}
