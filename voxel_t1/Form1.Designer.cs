﻿namespace voxel_t1
{
    partial class voxel_t1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btOpen = new System.Windows.Forms.Button();
            this.ofdX = new System.Windows.Forms.OpenFileDialog();
            this.numX = new System.Windows.Forms.NumericUpDown();
            this.numY = new System.Windows.Forms.NumericUpDown();
            this.numZ = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.numY_cur = new System.Windows.Forms.NumericUpDown();
            this.numGuard = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.btDraw = new System.Windows.Forms.Button();
            this.btClose = new System.Windows.Forms.Button();
            this.lblX = new System.Windows.Forms.Label();
            this.lblZ = new System.Windows.Forms.Label();
            this.lblY = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.numZ_cur = new System.Windows.Forms.NumericUpDown();
            this.numX_cur = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.numScale = new System.Windows.Forms.NumericUpDown();
            this.btSave = new System.Windows.Forms.Button();
            this.sfd = new System.Windows.Forms.SaveFileDialog();
            this.label11 = new System.Windows.Forms.Label();
            this.ofdY = new System.Windows.Forms.OpenFileDialog();
            this.ofdZ = new System.Windows.Forms.OpenFileDialog();
            this.ofdStatus = new System.Windows.Forms.OpenFileDialog();
            this.chkDrawMode = new System.Windows.Forms.CheckBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.bgDrawX = new System.ComponentModel.BackgroundWorker();
            this.pbX = new System.Windows.Forms.ProgressBar();
            this.bgSaveX = new System.ComponentModel.BackgroundWorker();
            this.lblPersentageX = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.clDialog = new System.Windows.Forms.ColorDialog();
            this.btColor = new System.Windows.Forms.Button();
            this.chkSaveMode = new System.Windows.Forms.CheckBox();
            this.pbY = new System.Windows.Forms.ProgressBar();
            this.pbZ = new System.Windows.Forms.ProgressBar();
            this.label13 = new System.Windows.Forms.Label();
            this.lblPersentageY = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblPersentageZ = new System.Windows.Forms.Label();
            this.bgSaveY = new System.ComponentModel.BackgroundWorker();
            this.bgSaveZ = new System.ComponentModel.BackgroundWorker();
            this.bgOpenX = new System.ComponentModel.BackgroundWorker();
            this.bgOpenY = new System.ComponentModel.BackgroundWorker();
            this.bgOpenZ = new System.ComponentModel.BackgroundWorker();
            this.bgDrawY = new System.ComponentModel.BackgroundWorker();
            this.bgDrawZ = new System.ComponentModel.BackgroundWorker();
            this.chkDrawDielectric = new System.Windows.Forms.CheckBox();
            this.chkXDraw = new System.Windows.Forms.CheckBox();
            this.chkYDraw = new System.Windows.Forms.CheckBox();
            this.chkZDraw = new System.Windows.Forms.CheckBox();
            this.pbImgZ = new InterpolatedPictureBox();
            this.pbImgX = new InterpolatedPictureBox();
            this.pbImgY = new InterpolatedPictureBox();
            this.lblSWX = new System.Windows.Forms.Label();
            this.lblSWY = new System.Windows.Forms.Label();
            this.lblSWZ = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numY_cur)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGuard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numZ_cur)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numX_cur)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImgZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImgX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImgY)).BeginInit();
            this.SuspendLayout();
            // 
            // btOpen
            // 
            this.btOpen.Location = new System.Drawing.Point(1226, 9);
            this.btOpen.Name = "btOpen";
            this.btOpen.Size = new System.Drawing.Size(75, 23);
            this.btOpen.TabIndex = 1;
            this.btOpen.Text = "開く";
            this.btOpen.UseVisualStyleBackColor = true;
            this.btOpen.Click += new System.EventHandler(this.btOpen_Click);
            // 
            // ofdX
            // 
            this.ofdX.FileName = "idx.dat";
            this.ofdX.RestoreDirectory = true;
            this.ofdX.Title = "Open Voxel File";
            // 
            // numX
            // 
            this.numX.Enabled = false;
            this.numX.Location = new System.Drawing.Point(11, 27);
            this.numX.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numX.Name = "numX";
            this.numX.Size = new System.Drawing.Size(120, 19);
            this.numX.TabIndex = 2;
            this.numX.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numX.ValueChanged += new System.EventHandler(this.numX_ValueChanged);
            // 
            // numY
            // 
            this.numY.Enabled = false;
            this.numY.Location = new System.Drawing.Point(137, 27);
            this.numY.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numY.Name = "numY";
            this.numY.Size = new System.Drawing.Size(120, 19);
            this.numY.TabIndex = 3;
            this.numY.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numY.ValueChanged += new System.EventHandler(this.numY_ValueChanged);
            // 
            // numZ
            // 
            this.numZ.Enabled = false;
            this.numZ.Location = new System.Drawing.Point(263, 27);
            this.numZ.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numZ.Name = "numZ";
            this.numZ.Size = new System.Drawing.Size(120, 19);
            this.numZ.TabIndex = 4;
            this.numZ.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numZ.ValueChanged += new System.EventHandler(this.numZ_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "X(Auto)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(135, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "Y(Auto)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(261, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "Z(Auto)";
            // 
            // numY_cur
            // 
            this.numY_cur.Location = new System.Drawing.Point(1129, 66);
            this.numY_cur.Name = "numY_cur";
            this.numY_cur.Size = new System.Drawing.Size(65, 19);
            this.numY_cur.TabIndex = 8;
            this.numY_cur.ValueChanged += new System.EventHandler(this.numY_cur_ValueChanged);
            // 
            // numGuard
            // 
            this.numGuard.Location = new System.Drawing.Point(389, 28);
            this.numGuard.Name = "numGuard";
            this.numGuard.Size = new System.Drawing.Size(120, 19);
            this.numGuard.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(387, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 10;
            this.label4.Text = "Guard Cell";
            // 
            // btDraw
            // 
            this.btDraw.Enabled = false;
            this.btDraw.Location = new System.Drawing.Point(1178, 9);
            this.btDraw.Name = "btDraw";
            this.btDraw.Size = new System.Drawing.Size(42, 23);
            this.btDraw.TabIndex = 11;
            this.btDraw.Text = "Draw";
            this.btDraw.UseVisualStyleBackColor = true;
            this.btDraw.Click += new System.EventHandler(this.btDraw_Click);
            // 
            // btClose
            // 
            this.btClose.Location = new System.Drawing.Point(1226, 38);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(75, 23);
            this.btClose.TabIndex = 12;
            this.btClose.Text = "閉じる";
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.Location = new System.Drawing.Point(309, 284);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(27, 12);
            this.lblX.TabIndex = 15;
            this.lblX.Text = "N/A";
            // 
            // lblZ
            // 
            this.lblZ.AutoSize = true;
            this.lblZ.Location = new System.Drawing.Point(106, 66);
            this.lblZ.Name = "lblZ";
            this.lblZ.Size = new System.Drawing.Size(27, 12);
            this.lblZ.TabIndex = 16;
            this.lblZ.Text = "N/A";
            // 
            // lblY
            // 
            this.lblY.AutoSize = true;
            this.lblY.Location = new System.Drawing.Point(104, 284);
            this.lblY.Name = "lblY";
            this.lblY.Size = new System.Drawing.Size(27, 12);
            this.lblY.TabIndex = 17;
            this.lblY.Text = "N/A";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 284);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 12);
            this.label5.TabIndex = 18;
            this.label5.Text = "Y視点";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(267, 284);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 12);
            this.label6.TabIndex = 19;
            this.label6.Text = "X視点";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 12);
            this.label7.TabIndex = 20;
            this.label7.Text = "Z視点";
            // 
            // numZ_cur
            // 
            this.numZ_cur.Location = new System.Drawing.Point(1218, 66);
            this.numZ_cur.Name = "numZ_cur";
            this.numZ_cur.Size = new System.Drawing.Size(65, 19);
            this.numZ_cur.TabIndex = 21;
            this.numZ_cur.ValueChanged += new System.EventHandler(this.numZ_cur_ValueChanged);
            // 
            // numX_cur
            // 
            this.numX_cur.Location = new System.Drawing.Point(1040, 66);
            this.numX_cur.Name = "numX_cur";
            this.numX_cur.Size = new System.Drawing.Size(65, 19);
            this.numX_cur.TabIndex = 22;
            this.numX_cur.ValueChanged += new System.EventHandler(this.numX_cur_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1200, 68);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 12);
            this.label8.TabIndex = 25;
            this.label8.Text = "Z";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1111, 68);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 12);
            this.label9.TabIndex = 24;
            this.label9.Text = "Y";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1022, 68);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(12, 12);
            this.label10.TabIndex = 23;
            this.label10.Text = "X";
            // 
            // numScale
            // 
            this.numScale.Location = new System.Drawing.Point(1328, 66);
            this.numScale.Name = "numScale";
            this.numScale.Size = new System.Drawing.Size(65, 19);
            this.numScale.TabIndex = 26;
            this.numScale.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numScale.ValueChanged += new System.EventHandler(this.numScale_ValueChanged);
            // 
            // btSave
            // 
            this.btSave.Enabled = false;
            this.btSave.Location = new System.Drawing.Point(1307, 9);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(106, 23);
            this.btSave.TabIndex = 27;
            this.btSave.Text = "PNG書き出し";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // sfd
            // 
            this.sfd.FileOk += new System.ComponentModel.CancelEventHandler(this.sfd_FileOk);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1289, 68);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 12);
            this.label11.TabIndex = 28;
            this.label11.Text = "Scale";
            // 
            // ofdY
            // 
            this.ofdY.FileName = "idy.dat";
            this.ofdY.RestoreDirectory = true;
            this.ofdY.Title = "Open Voxel File";
            // 
            // ofdZ
            // 
            this.ofdZ.FileName = "idz.dat";
            this.ofdZ.RestoreDirectory = true;
            this.ofdZ.Title = "Open Voxel File";
            // 
            // ofdStatus
            // 
            this.ofdStatus.FileName = "status.dat";
            this.ofdStatus.RestoreDirectory = true;
            this.ofdStatus.Title = "Open Voxel File";
            // 
            // chkDrawMode
            // 
            this.chkDrawMode.AutoSize = true;
            this.chkDrawMode.Checked = true;
            this.chkDrawMode.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDrawMode.Location = new System.Drawing.Point(1040, 20);
            this.chkDrawMode.Name = "chkDrawMode";
            this.chkDrawMode.Size = new System.Drawing.Size(72, 16);
            this.chkDrawMode.TabIndex = 29;
            this.chkDrawMode.Text = "全体描画";
            this.chkDrawMode.UseVisualStyleBackColor = true;
            this.chkDrawMode.CheckedChanged += new System.EventHandler(this.chkDrawMode_CheckedChanged);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // bgDrawX
            // 
            this.bgDrawX.WorkerReportsProgress = true;
            this.bgDrawX.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgDraw_DoWork);
            this.bgDrawX.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgDraw_ProgressChanged);
            this.bgDrawX.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgDraw_RunWorkerCompleted);
            // 
            // pbX
            // 
            this.pbX.Location = new System.Drawing.Point(522, 12);
            this.pbX.Name = "pbX";
            this.pbX.Size = new System.Drawing.Size(466, 10);
            this.pbX.TabIndex = 30;
            // 
            // bgSaveX
            // 
            this.bgSaveX.WorkerReportsProgress = true;
            this.bgSaveX.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgSave_DoWork);
            this.bgSaveX.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgSave_ProgressChanged);
            this.bgSaveX.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgSave_RunWorkerCompleted);
            // 
            // lblPersentageX
            // 
            this.lblPersentageX.AutoSize = true;
            this.lblPersentageX.Location = new System.Drawing.Point(994, 9);
            this.lblPersentageX.Name = "lblPersentageX";
            this.lblPersentageX.Size = new System.Drawing.Size(11, 12);
            this.lblPersentageX.TabIndex = 31;
            this.lblPersentageX.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(1023, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(11, 12);
            this.label12.TabIndex = 32;
            this.label12.Text = "%";
            // 
            // btColor
            // 
            this.btColor.Location = new System.Drawing.Point(1307, 38);
            this.btColor.Name = "btColor";
            this.btColor.Size = new System.Drawing.Size(106, 23);
            this.btColor.TabIndex = 33;
            this.btColor.Text = "背景色変更";
            this.btColor.UseVisualStyleBackColor = true;
            this.btColor.Click += new System.EventHandler(this.btColor_Click);
            // 
            // chkSaveMode
            // 
            this.chkSaveMode.AutoSize = true;
            this.chkSaveMode.Location = new System.Drawing.Point(1040, 42);
            this.chkSaveMode.Name = "chkSaveMode";
            this.chkSaveMode.Size = new System.Drawing.Size(180, 16);
            this.chkSaveMode.TabIndex = 34;
            this.chkSaveMode.Text = "全体描画時全視点ポイント保存";
            this.chkSaveMode.UseVisualStyleBackColor = true;
            // 
            // pbY
            // 
            this.pbY.Location = new System.Drawing.Point(522, 28);
            this.pbY.Name = "pbY";
            this.pbY.Size = new System.Drawing.Size(466, 10);
            this.pbY.TabIndex = 35;
            // 
            // pbZ
            // 
            this.pbZ.Location = new System.Drawing.Point(522, 44);
            this.pbZ.Name = "pbZ";
            this.pbZ.Size = new System.Drawing.Size(466, 10);
            this.pbZ.TabIndex = 36;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1023, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(11, 12);
            this.label13.TabIndex = 38;
            this.label13.Text = "%";
            // 
            // lblPersentageY
            // 
            this.lblPersentageY.AutoSize = true;
            this.lblPersentageY.Location = new System.Drawing.Point(994, 26);
            this.lblPersentageY.Name = "lblPersentageY";
            this.lblPersentageY.Size = new System.Drawing.Size(11, 12);
            this.lblPersentageY.TabIndex = 37;
            this.lblPersentageY.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(1023, 43);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(11, 12);
            this.label15.TabIndex = 40;
            this.label15.Text = "%";
            // 
            // lblPersentageZ
            // 
            this.lblPersentageZ.AutoSize = true;
            this.lblPersentageZ.Location = new System.Drawing.Point(994, 43);
            this.lblPersentageZ.Name = "lblPersentageZ";
            this.lblPersentageZ.Size = new System.Drawing.Size(11, 12);
            this.lblPersentageZ.TabIndex = 39;
            this.lblPersentageZ.Text = "0";
            // 
            // bgSaveY
            // 
            this.bgSaveY.WorkerReportsProgress = true;
            this.bgSaveY.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgSaveY_DoWork);
            this.bgSaveY.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgSaveY_ProgressChanged);
            this.bgSaveY.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgSaveY_RunWorkerCompleted);
            // 
            // bgSaveZ
            // 
            this.bgSaveZ.WorkerReportsProgress = true;
            this.bgSaveZ.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgSaveZ_DoWork);
            this.bgSaveZ.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgSaveZ_ProgressChanged);
            this.bgSaveZ.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgSaveZ_RunWorkerCompleted);
            // 
            // bgOpenX
            // 
            this.bgOpenX.WorkerReportsProgress = true;
            this.bgOpenX.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgOpenX_DoWork);
            this.bgOpenX.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgOpenX_ProgressChanged);
            this.bgOpenX.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgOpenX_RunWorkerCompleted);
            // 
            // bgOpenY
            // 
            this.bgOpenY.WorkerReportsProgress = true;
            this.bgOpenY.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgOpenY_DoWork);
            this.bgOpenY.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgOpenY_ProgressChanged);
            this.bgOpenY.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgOpenY_RunWorkerCompleted);
            // 
            // bgOpenZ
            // 
            this.bgOpenZ.WorkerReportsProgress = true;
            this.bgOpenZ.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgOpenZ_DoWork);
            this.bgOpenZ.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgOpenZ_ProgressChanged);
            this.bgOpenZ.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgOpenZ_RunWorkerCompleted);
            // 
            // bgDrawY
            // 
            this.bgDrawY.WorkerReportsProgress = true;
            this.bgDrawY.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgDrawY_DoWork);
            this.bgDrawY.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgDrawY_ProgressChanged);
            this.bgDrawY.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgDrawY_RunWorkerCompleted);
            // 
            // bgDrawZ
            // 
            this.bgDrawZ.WorkerReportsProgress = true;
            this.bgDrawZ.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgDrawZ_DoWork);
            this.bgDrawZ.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgDrawZ_ProgressChanged);
            this.bgDrawZ.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgDrawZ_RunWorkerCompleted);
            // 
            // chkDrawDielectric
            // 
            this.chkDrawDielectric.AutoSize = true;
            this.chkDrawDielectric.Checked = true;
            this.chkDrawDielectric.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDrawDielectric.Location = new System.Drawing.Point(1040, 1);
            this.chkDrawDielectric.Name = "chkDrawDielectric";
            this.chkDrawDielectric.Size = new System.Drawing.Size(84, 16);
            this.chkDrawDielectric.TabIndex = 41;
            this.chkDrawDielectric.Text = "誘電体描画";
            this.chkDrawDielectric.UseVisualStyleBackColor = true;
            // 
            // chkXDraw
            // 
            this.chkXDraw.AutoSize = true;
            this.chkXDraw.Checked = true;
            this.chkXDraw.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkXDraw.Location = new System.Drawing.Point(1024, 91);
            this.chkXDraw.Name = "chkXDraw";
            this.chkXDraw.Size = new System.Drawing.Size(79, 16);
            this.chkXDraw.TabIndex = 42;
            this.chkXDraw.Text = "X視点描画";
            this.chkXDraw.UseVisualStyleBackColor = true;
            // 
            // chkYDraw
            // 
            this.chkYDraw.AutoSize = true;
            this.chkYDraw.Checked = true;
            this.chkYDraw.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkYDraw.Location = new System.Drawing.Point(1113, 91);
            this.chkYDraw.Name = "chkYDraw";
            this.chkYDraw.Size = new System.Drawing.Size(79, 16);
            this.chkYDraw.TabIndex = 43;
            this.chkYDraw.Text = "Y視点描画";
            this.chkYDraw.UseVisualStyleBackColor = true;
            // 
            // chkZDraw
            // 
            this.chkZDraw.AutoSize = true;
            this.chkZDraw.Checked = true;
            this.chkZDraw.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkZDraw.Location = new System.Drawing.Point(1202, 91);
            this.chkZDraw.Name = "chkZDraw";
            this.chkZDraw.Size = new System.Drawing.Size(79, 16);
            this.chkZDraw.TabIndex = 44;
            this.chkZDraw.Text = "Z視点描画";
            this.chkZDraw.UseVisualStyleBackColor = true;
            // 
            // pbImgZ
            // 
            this.pbImgZ.BackColor = System.Drawing.Color.Cyan;
            this.pbImgZ.Interpolation = System.Drawing.Drawing2D.InterpolationMode.Default;
            this.pbImgZ.Location = new System.Drawing.Point(14, 81);
            this.pbImgZ.Name = "pbImgZ";
            this.pbImgZ.Size = new System.Drawing.Size(200, 200);
            this.pbImgZ.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbImgZ.TabIndex = 14;
            this.pbImgZ.TabStop = false;
            // 
            // pbImgX
            // 
            this.pbImgX.BackColor = System.Drawing.Color.Cyan;
            this.pbImgX.Interpolation = System.Drawing.Drawing2D.InterpolationMode.Default;
            this.pbImgX.Location = new System.Drawing.Point(261, 299);
            this.pbImgX.Name = "pbImgX";
            this.pbImgX.Size = new System.Drawing.Size(200, 200);
            this.pbImgX.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbImgX.TabIndex = 13;
            this.pbImgX.TabStop = false;
            // 
            // pbImgY
            // 
            this.pbImgY.BackColor = System.Drawing.Color.Cyan;
            this.pbImgY.Interpolation = System.Drawing.Drawing2D.InterpolationMode.Default;
            this.pbImgY.Location = new System.Drawing.Point(12, 299);
            this.pbImgY.Name = "pbImgY";
            this.pbImgY.Size = new System.Drawing.Size(200, 200);
            this.pbImgY.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbImgY.TabIndex = 0;
            this.pbImgY.TabStop = false;
            // 
            // lblSWX
            // 
            this.lblSWX.AutoSize = true;
            this.lblSWX.Location = new System.Drawing.Point(1326, 92);
            this.lblSWX.Name = "lblSWX";
            this.lblSWX.Size = new System.Drawing.Size(26, 12);
            this.lblSWX.TabIndex = 45;
            this.lblSWX.Text = "0ms";
            // 
            // lblSWY
            // 
            this.lblSWY.AutoSize = true;
            this.lblSWY.Location = new System.Drawing.Point(1326, 104);
            this.lblSWY.Name = "lblSWY";
            this.lblSWY.Size = new System.Drawing.Size(26, 12);
            this.lblSWY.TabIndex = 46;
            this.lblSWY.Text = "0ms";
            // 
            // lblSWZ
            // 
            this.lblSWZ.AutoSize = true;
            this.lblSWZ.Location = new System.Drawing.Point(1326, 116);
            this.lblSWZ.Name = "lblSWZ";
            this.lblSWZ.Size = new System.Drawing.Size(26, 12);
            this.lblSWZ.TabIndex = 47;
            this.lblSWZ.Text = "0ms";
            // 
            // voxel_t1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1429, 745);
            this.Controls.Add(this.lblSWZ);
            this.Controls.Add(this.lblSWY);
            this.Controls.Add(this.lblSWX);
            this.Controls.Add(this.chkZDraw);
            this.Controls.Add(this.chkYDraw);
            this.Controls.Add(this.chkXDraw);
            this.Controls.Add(this.chkDrawDielectric);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.lblPersentageZ);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.lblPersentageY);
            this.Controls.Add(this.pbZ);
            this.Controls.Add(this.pbY);
            this.Controls.Add(this.chkSaveMode);
            this.Controls.Add(this.btColor);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lblPersentageX);
            this.Controls.Add(this.pbX);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.numY_cur);
            this.Controls.Add(this.numScale);
            this.Controls.Add(this.numX_cur);
            this.Controls.Add(this.numZ_cur);
            this.Controls.Add(this.chkDrawMode);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.btClose);
            this.Controls.Add(this.btDraw);
            this.Controls.Add(this.btOpen);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblY);
            this.Controls.Add(this.lblZ);
            this.Controls.Add(this.lblX);
            this.Controls.Add(this.pbImgZ);
            this.Controls.Add(this.pbImgX);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numGuard);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numZ);
            this.Controls.Add(this.numY);
            this.Controls.Add(this.numX);
            this.Controls.Add(this.pbImgY);
            this.Name = "voxel_t1";
            this.Text = "Main";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numY_cur)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGuard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numZ_cur)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numX_cur)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImgZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImgX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImgY)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private InterpolatedPictureBox pbImgY;
        private System.Windows.Forms.Button btOpen;
        private System.Windows.Forms.OpenFileDialog ofdX;
        private System.Windows.Forms.NumericUpDown numX;
        private System.Windows.Forms.NumericUpDown numY;
        private System.Windows.Forms.NumericUpDown numZ;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numY_cur;
        private System.Windows.Forms.NumericUpDown numGuard;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btDraw;
		private System.Windows.Forms.Button btClose;
		private InterpolatedPictureBox pbImgX;
		private InterpolatedPictureBox pbImgZ;
		private System.Windows.Forms.Label lblX;
		private System.Windows.Forms.Label lblZ;
		private System.Windows.Forms.Label lblY;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.NumericUpDown numZ_cur;
		private System.Windows.Forms.NumericUpDown numX_cur;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.NumericUpDown numScale;
		private System.Windows.Forms.Button btSave;
		private System.Windows.Forms.SaveFileDialog sfd;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.OpenFileDialog ofdY;
		private System.Windows.Forms.OpenFileDialog ofdStatus;
		private System.Windows.Forms.OpenFileDialog ofdZ;
		private System.Windows.Forms.CheckBox chkDrawMode;
		private System.Windows.Forms.ImageList imageList1;
		private System.ComponentModel.BackgroundWorker bgDrawX;
		private System.Windows.Forms.ProgressBar pbX;
		private System.ComponentModel.BackgroundWorker bgSaveX;
		private System.Windows.Forms.Label lblPersentageX;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.ColorDialog clDialog;
		private System.Windows.Forms.Button btColor;
		private System.Windows.Forms.CheckBox chkSaveMode;
		private System.Windows.Forms.ProgressBar pbY;
		private System.Windows.Forms.ProgressBar pbZ;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label lblPersentageY;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label lblPersentageZ;
		private System.ComponentModel.BackgroundWorker bgSaveY;
		private System.ComponentModel.BackgroundWorker bgSaveZ;
		private System.ComponentModel.BackgroundWorker bgOpenX;
		private System.ComponentModel.BackgroundWorker bgOpenY;
		private System.ComponentModel.BackgroundWorker bgOpenZ;
		private System.ComponentModel.BackgroundWorker bgDrawY;
		private System.ComponentModel.BackgroundWorker bgDrawZ;
		private System.Windows.Forms.CheckBox chkDrawDielectric;
		private System.Windows.Forms.CheckBox chkXDraw;
		private System.Windows.Forms.CheckBox chkYDraw;
		private System.Windows.Forms.CheckBox chkZDraw;
		private System.Windows.Forms.Label lblSWX;
		private System.Windows.Forms.Label lblSWY;
		private System.Windows.Forms.Label lblSWZ;
    }
}

